﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="PhoneLookup.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <META NAME="robots" CONTENT="noindex,nofollow">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-area">
            <div class="container">
                <div class="row page-title text-center wow zoomInDown" data-wow-delay="0.1s">
                  <h1>Register Now</h1><br/>
                </div>
                       <form class="form-horizontal">



<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="radios">Membership Options</label>
  <div class="col-md-4">
  <div class="radio">
    <label class="" for="prod1">
      <input type="radio" name="prod1" id="prod1" value="1">
      <span style="color: green; font-weight: bold">Premium Membership</span>
                    - Full results + Unlimited People Searches for 1 Year ($23.95) -
                    <span style="color: red; font-weight: bold">Best Value!</span>
    </label>
	</div>
  <div class="radio">
    <label for="prod2">
      <input type="radio" name="prod2" id="prod2" value="2">
      <span style="color: green; font-weight: bold">Limited Membership</span>
                    Full results for <%=this.phone%> ($0.95)
    </label>
	</div>

  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button class="navbar-btn nav-button wow bounceInRight login" data-wow-delay="0.8s" onclick="registerNow();return false">Checkout</button>
  </div>
</div>
</form>

                </div>
            </div>

</asp:Content>
