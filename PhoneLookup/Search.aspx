﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="PhoneLookup.Search"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <META NAME="robots" CONTENT="noindex,nofollow">
    <title>Phone Lookup Search</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div style="text-align:center">
          <h1>Searching...</h1>
          <p>
            <img alt="wait" src="Scripts/img/processingbar.gif" />
          </p>
          <p>
            <br />
				Please wait a moment. If this page does not refresh, <a href="/Results.aspx?phone=<%=this.phone%>">click here</a>.
			</p>
   </div>
<script>
        setTimeout(function () {
            window.location.href = "/Results.aspx?phone=<%=this.phone%>"
            }, 5000);
    </script>

      <div class="footer-area" style="width:100%;height:100px;bottom:0;left:0;position:absolute">
            <div class="container">
                <div class="row footer-copy">
                    <p style="text-align:center">
					<span><a href="/Default.aspx">Home</a></span>
					| <span><a href="http://www.reversephonecheck.com/terms.php" target="_blank">Terms of Use</a></span>
                    | <span><a href="http://www.reversephonecheck.com/privacy.php" target="_blank">Privacy policy</a></span>
					| <span><a href="http://new-members.reversephonecheck.com/customer/help" target="_blank"> Contact</a></span> 
                    | <span><a href="http://members.reversephonecheck.com/customer" target="_blank">Login</a></span>
					</p>
                    <p style="text-align:center">
					    © 2011 - <%= DateTime.Now.Year.ToString() %>  PhoneLookupSearch.com
					</p>
                </div>
            </div>
        </div>
</asp:Content>
