using PhoneLookup.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhoneLookup
{
    public partial class Results : System.Web.UI.Page
    {
        public string phone;
        public string location; //country, state
        public string provider;
        public string carrier;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            carrier = Session["carrier"] as string; //user type ,mobile phone or landline
            provider = Session["provider"] as string;
            string country = Session["country"] as string;
            string state = Session["state"] as string;

            if(String.IsNullOrEmpty(carrier) && String.IsNullOrEmpty(provider) && String.IsNullOrEmpty(country) && String.IsNullOrEmpty(state))
            {
                Response.Redirect("NoResults.aspx");
            }

            location = country + ", " + state;
          
            if (Request.QueryString["phone"] != null)
            {
                phone = Request.QueryString["phone"].ToString();
            }
            else
            {
                Response.Redirect("NoResults.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Register.aspx?phone=" + phone);
        }
    }
}