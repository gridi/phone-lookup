﻿using PhoneLookup.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PhoneLookup
{
    public partial class Search : System.Web.UI.Page
    {
        public string phone;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["phone"] != null)
                {
                    phone = Request.QueryString["phone"].ToString();
                    Phone p = new Phone();
                    p.phone_number = phone;

                    Report[] arr = Phone.LookupNumber(p);
                    Report r = arr[0];

                    if (arr != null)
                    {
                        Session["city"] = r.city;
                        Session["state"] = r.state;
                        Session["country"] = r.country;
                        Session["timezone"] = r.timezone;
                        Session["provider"] = r.provider;
                        Session["carrier"] = r.carrier;
                    }
                }
                else
                {
                    Response.Redirect("NoResults.aspx?phone=" + phone);
                }
            }

            catch (Exception ex)
            {
                Response.Redirect("NoResults.aspx");
            }
            finally
            { 

            }
        }
    }
}