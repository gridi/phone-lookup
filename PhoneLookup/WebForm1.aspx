﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="PhoneLookup.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        

<div class='row recently-searched' >
	<h6 class='center-title'>Recently Searched Phone Numbers</h6>

	<div class='columns large-3 medium-3'>
			<a href='/search/results?phone=(304)-257-9730'>(304)-257-9730</a><br />
			<a href='/search/results?phone=(313)-792-9863'>(313)-792-9863</a><br />
			<a href='/search/results?phone=(205)-499-5432'>(205)-499-5432</a><br />
			<a href='/search/results?phone=(860)-973-1626'>(860)-973-1626</a><br />
		</div>
	<div class='columns large-3 medium-3'>
			<a href='/search/results?phone=(281)-554-3727'>(281)-554-3727</a><br />
			<a href='/search/results?phone=(714)-408-9334'>(714)-408-9334</a><br />
			<a href='/search/results?phone=(713)-463-7413'>(713)-463-7413</a><br />
			<a href='/search/results?phone=(410)-444-7693'>(410)-444-7693</a><br />
		</div>	
	<div class='columns large-3 medium-3'>
			<a href='/search/results?phone=(318)-222-6436'>(318)-222-6436</a><br />
			<a href='/search/results?phone=(816)-450-8632'>(816)-450-8632</a><br />
			<a href='/search/results?phone=(609)-585-9849'>(609)-585-9849</a><br />
			<a href='/search/results?phone=(845)-486-9583'>(845)-486-9583</a><br />
		</div>
	<div class='columns large-3 medium-3'>
			<a href='/search/results?phone=(210)-481-6766'>(210)-481-6766</a><br />
			<a href='/search/results?phone=(708)-547-7983'>(708)-547-7983</a><br />
			<a href='/search/results?phone=(623)-742-7465'>(623)-742-7465</a><br />
			<a href='/recent'>view more...</a>
	</div>
</div>



    </div>
    </form>
</body>
</html>
