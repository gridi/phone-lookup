﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Results.aspx.cs" Inherits="PhoneLookup.Results" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <META NAME="robots" CONTENT="noindex,nofollow">
    <title>Phone Lookup Search</title>
    <style> td { vertical-align: middle; } </style>
   
     <script src = "http://maps.googleapis.com/maps/api/js"></script>
  
        <script>
            var geocoder = new google.maps.Geocoder();
            var address = "<%=this.location%>";

            geocoder.geocode({'address': address }, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    initialize(latitude, longitude);
                }
            });


            function initialize(latitude, longitude) {
                var latlng = new google.maps.LatLng(latitude, longitude);
                var myOptions = {
                    zoom: 14,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false,
                    draggable: false,
                    mapTypeControl: false
                };
                var map = new google.maps.Map(document.getElementById("map"), myOptions);

                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    title: ""
                });
                var contentString = '';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });
            }
        </script>
               
        <script>var date = new Date(); var n = date.toDateString(); var time = date.toLocaleTimeString(); document.getElementById('time').innerHTML = n + ', ' + time;</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="content-area">
            <div class="container">
                <div class="row page-title text-center wow zoomInDown" data-wow-delay="0.1s">
                  <h5>Search results for number <%=this.phone%></h5>
                </div>

<table class="table-responsive">
          <tr>
            <td>
              <strong>Name</strong>:</td>
            <td><a href="Register.aspx?phone=<%=this.phone%>">
                <strong>Click Here</strong></a></td>
            <td rowspan="5">
              <div id="map" style = "width:270px; height:280px;">
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <strong>Location</strong>:</td>
            <td><%=this.location%></td>
          </tr>
           <tr>
            <td>
              <strong>Full Address</strong>:</td>
            <td>
                <a href="Register.aspx?phone=<%=this.phone%>">
                <strong>Click Here</strong>
              </a>
           </td>
          </tr>
           <tr>
            <td>
              <strong>Service Type</strong>:</td>
            <td><%=this.carrier%></td>
          </tr>
          <tr>
            <td>
              <strong>Service Provider</strong>:</td>
            <td><%=this.provider%></td>
          </tr>
        <!--  <tr>
            <td>
              <strong>Search Date</strong>:</td>
            <td id="time"></td>
          </tr> -->
          <tr>
            <td>
              <strong>Full Report</strong>:</td>
            <td>
              <a href="Register.aspx?phone=<%=this.phone%>">
                <strong>Click Here</strong>
              </a>
            </td>
          </tr>
          <tr>
              
            <td colspan="2"">
                <hr/>
			* Full Report contains the owner's full name and information<br/>
                 about filed complaints, people search results and more! 
            </td>
          </tr>
        </table>
<table>
           <tr>
            <td colspan="2">
                <asp:Button ID="Button1" runat="server" Text="Register Now" CssClass="navbar-btn nav-button wow bounceInRight login" OnClick="Button1_Click"  />
            </td>
          </tr>
   </table>
                </div>
            </div>


       <div class="footer-area" style="width:100%;height:100px;bottom:0;left:0;">
            <div class="container">
                <div class="row footer-copy">
                    <p style="text-align:center">
					<span><a href="/Default.aspx">Home</a></span>
					| <span><a href="http://www.reversephonecheck.com/terms.php" target="_blank">Terms of Use</a></span>
                    | <span><a href="http://www.reversephonecheck.com/privacy.php" target="_blank">Privacy policy</a></span>
					| <span><a href="http://new-members.reversephonecheck.com/customer/help" target="_blank"> Contact</a></span> 
                    | <span><a href="http://members.reversephonecheck.com/customer" target="_blank">Login</a></span>
					</p>
                    <p style="text-align:center">
					    © 2011 - <%= DateTime.Now.Year.ToString() %>  PhoneLookupSearch.com
					</p>
                </div>
            </div>
        </div>
</asp:Content>
