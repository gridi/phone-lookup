﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeFile="NoResults.aspx.cs" Inherits="PhoneLookup.NoResults" %>
<%@ Register Src="~/PhoneUserControl1.ascx" TagName="SearchCtrl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <META NAME="robots" CONTENT="noindex,nofollow">
    <title>Phone Lookup Search</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="text-align:center">
          <h1> No results found for your search</h1>
          <br />
          <p>
            Use the form below to search for another Phone Number<br/>
              <uc1:SearchCtrl runat="server" />
			</p>
   </div>
</asp:Content>
