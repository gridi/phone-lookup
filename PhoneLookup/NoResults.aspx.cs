﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhoneLookup
{
    public partial class NoResults : System.Web.UI.Page
    {
        public string phone;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["phone"] != null)
            {
                phone = Request.QueryString["phone"].ToString().Trim();
            }
        }
    }
}