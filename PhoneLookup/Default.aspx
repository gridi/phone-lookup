﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PhoneLookup.Default" %>
<%@ Register Src="~/PhoneUserControl1.ascx" TagName="SearchCtrl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
     <meta name='keywords' content='reverse phone lookup,reverse phone directory,reverse phone search,reverse phone number
            ,reverse phone,reverse phone number lookup,reverse phone numbers,reverse phone look up,reverse phone book,reverse
             phone number look up,phone number reverse,phone number reverse search,reverse lookup phone number,reverse cell phone
             number lookup,cell phone reverse,phone reverse,reverse phone exact, find phone number owner, identify caller' />
     <meta name='description' content="Who is Calling You from this Number? Find Out The Owner's Name and Address" />
    <title>Phone Lookup Search</title>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="slider-area">
          <asp:Panel ID="Panel1" runat="server" CssClass="slider">
              <div id="bg-slider" class="owl-carousel owl-theme">
                  <div class="item"><img src="Scripts/img/bg.jpg" alt="The Last of us" /></div>
               </div>
          </asp:Panel>
            <div class="container slider-content">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                        <h2>Reverse Phone Lookup for Any Number</h2>
                        <p id="para">Results include name, address, carrier, and other details when available. Your search is confidential.</p>
                         <uc1:SearchCtrl runat="server" />
                    </div>
                </div>
            </div>
        </div>
          <div class="content-area">
            <div class="container">
                <div class="row how-it-work text-center">
                    <div class="col-md-4">
                        <div class="single-work wow fadeInUp" data-wow-delay="0.8s">
                            <img src="Scripts/img/how-work1.png" alt="">
                            <h3>Our Service</h3>
                            <p>
                                Our service offers access to premium data sources in order to find out information about ANY 
                                phone number you are searching to. This information might not be included in publicly available
                                directories. Furthermore these directories do not usually include cell phone numbers.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-work  wow fadeInUp"  data-wow-delay="0.9s">
                            <img src="Scripts/img/how-work2.png" alt="">
                            <h3>Frequent Updates</h3>
                            <p>We continuously update our database to
provide the most accurate names,
addresses, and more.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-work wow fadeInUp"  data-wow-delay="1s">
                            <img src="Scripts/img/how-work3.png" alt="">
                            <h3>Guaranted Results</h3>
                            <p>Our Premium Data Sources contain over 300 million phone (cell or landline) and unlisted numbers. Use cases may include researching a phone bill, customer address veryfing, identify unwanted callers, indeed endless possibilities.</p>
                        </div>
                    </div>
                </div>
            </div>

         <div class="footer-area" style="width:100%;height:100px;bottom:0;left:0;">
            <div class="container">
                <div class="row footer-copy">
                    <p style="text-align:center">
					<span><a href="/Default.aspx">Home</a></span>
					| <span><a href="http://www.reversephonecheck.com/terms.php" target="_blank">Terms of Use</a></span>
                    | <span><a href="http://www.reversephonecheck.com/privacy.php" target="_blank">Privacy policy</a></span>
					| <span><a href="http://new-members.reversephonecheck.com/customer/help" target="_blank"> Contact</a></span> 
                    | <span><a href="http://members.reversephonecheck.com/customer" target="_blank">Login</a></span>
					</p>
                    <p style="text-align:center">
					    © 2011 - <%= DateTime.Now.Year.ToString() %>  PhoneLookupSearch.com
					</p>
                </div>
            </div>
        </div>
</asp:Content>


  
