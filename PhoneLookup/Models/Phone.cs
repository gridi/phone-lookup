﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace PhoneLookup.Models
{
    public class Phone
    {
      //  private String _phone_number;

        public String phone_number { get; set; }

        public static Report[] LookupNumber(Phone p)
        {
            List<Report> lst = new List<Report>();
            WebClient webclient = new WebClient();
            HtmlDocument htmlDoc = new HtmlDocument();
           
            htmlDoc.Load(webclient.OpenRead("http://www.free-reverse-cell-phone-directory.com/phonelookup/" + p.phone_number.Trim() + ".aspx"), Encoding.UTF8);

            lst.Add(new Report()
            {
                city = htmlDoc.DocumentNode.SelectSingleNode("//table[@cellpadding='3']/tr[7]/td[2]").InnerText,
                state = htmlDoc.DocumentNode.SelectSingleNode("//table[@cellpadding='3']/tr[8]/td[2]").InnerText,
                country = htmlDoc.DocumentNode.SelectSingleNode("//table[@cellpadding='3']/tr[9]/td[2]").InnerText,
                timezone = htmlDoc.DocumentNode.SelectSingleNode("//table[@cellpadding='3']/tr[11]/td[2]").InnerText,
                provider = htmlDoc.DocumentNode.SelectSingleNode("//table[@cellpadding='3']/tr[12]/td[2]").InnerText,
                carrier = htmlDoc.DocumentNode.SelectSingleNode("//table[@cellpadding='3']/tr[13]/td[2]").InnerText, //user type
            });
            return lst.ToArray();
        }
    }
}