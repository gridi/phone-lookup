﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace PhoneLookup.Models
{
    public class Report
    {
        private String _city;
        private String _state;
        private String _country;
        private String _timezone;
        private String _provider;
        private String _carrier;

        public Report()
        {
            _city = String.Empty;
            _state = String.Empty;
            _country = String.Empty;
            _timezone = String.Empty;
            _provider = String.Empty;
            _carrier = String.Empty;
        }

        public String city { get; set; }
        public String state { get; set; }
        public String country { get; set; }
        public String timezone { get; set; }
        public String provider { get; set; }
        public String carrier { get; set; }
    }
}